package euler.problem1;

public class Problem1 {

	/**
	 * @param args
	 * If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. 
	 * The sum of these multiples is 23.
	 * 
	 * Find the sum of all the multiples of 3 or 5 below 1000.
	 */
	public static void main(String[] args) {
		int sum=0;
		long startTime = System.nanoTime();
		for (int i=1;i<1000;i++) {
			if ((i%3==0)||(i%5==0)) {
				sum+=i;				
			}
		}
		long endTime = System.nanoTime();
		System.out.println("**"+sum);
		System.out.println("elapsed: " + (endTime-startTime) + "ns ");
		//answer = 233168
	}

}
