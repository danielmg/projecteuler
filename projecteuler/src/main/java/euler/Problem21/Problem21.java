package euler.Problem21;


public class Problem21 {

	/**
	 * 
     * Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
	 * If d(a) = b and d(b) = a, where a != b, then a and b are an amicable pair and each of a and b are called amicable numbers.
	 * 
	 * For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
	 * 
	 * Evaluate the sum of all the amicable numbers under 10000.
	 */
	private static final int MAX_VAL = 10000;
	
	
	//answer: 31626
	public static void main(String[] args) {
		long startTime = System.nanoTime();
		int sum=0;
		for (int a=1;a<MAX_VAL;a++) {		
			int d = d(a);
			for (int b=a+1;b<MAX_VAL;b++) {
				if (d==b&&d(b)==a) {
					sum+=(a+b);
				}
			}
		}
		long endTime = System.nanoTime();
		System.out.println("elapsed: " + (endTime-startTime) + "ns ");
		
		System.out.println(sum);
	}

	private static int d(int n) {
		int ret=1;
		int max=n/2;
		for (int i=2;i<max;i++) {
			if (n%i==0) {
				max=n/i;
				ret+=(i+max);
			}
		}
		return ret;
	}
	
}
