package euler.problem41;

import java.util.TreeSet;

public class Problem41 {

	// can only be max of 7 digits long as the max pandigital is 9 numbers but 8
	// and 9 len pandigital numbers are not prime - any 3 digits add up to a number divisable by 3 
	//therefore because 8 and 9 are not prime the resulting number cannot be.
	private static final int MAX_NUM = 7654321;

	//for the permutation version
	private static final int MAX_SIZE= 7;
	private static final int MIN_SIZE = 4;
	
	//answer        : 7652413
	//second largest: 7642513
	
	public static void main(String[] args) {
		//two different versions;
		v1();
		v2();
	}

	// first attempt - brute force check of numbers
	private static final void v1() {
		long startTime = System.nanoTime();
		int answer = MAX_NUM;
		int res=0;
		for (int i = MAX_NUM; i > 0; i--) {
			answer = i;
			if (isPanDigital(answer) && isPrime(answer)) {
				System.out.println(answer);
				if (++res>0) break;
			}
		}

		long endTime = System.nanoTime();
		System.out.println("v1 answer:" + answer);
		System.out.println("v1 elapsed: " + (endTime - startTime) + "ns ");
	}

	// second attempt using permutations
	private static final void v2() {

		long startTime = System.nanoTime();
		int res=-1;
		for (int x = MAX_SIZE; x >= MIN_SIZE; x--) {
			TreeSet<Integer> s = generatePanDigitals(x);		
			for (int i: s.descendingSet()) {
				if (isPrime(i)) {
					res=i;
					break;
				}
			}
			if (res>0) break;
		}
		long endTime = System.nanoTime();
		System.out.println("v2 answer:" + res);
		System.out.println("v2 elapsed: " + (endTime - startTime) + "ns ");

	}

	// basic brute force prime check for numbers of 6k+1 and 6k-1
	private static boolean isPrime(int number) {
		if (number == 1 || number % 2 == 0 || number % 3 == 0) return false;
		for (int i = 5; i <= Math.sqrt(number); i += 6) {
			if (number % i == 0 || number % (i + 2) == 0) 	return false;			
		}
		return true;
	}

	// generate all the possible "pandigitals" (permutations) for a given length
	private static TreeSet<Integer>  generatePanDigitals(int size) {		
		//the results will be n! big.
		char[] c = new char[size];

		// create the ascending string - e.g 12345
		for (int x = 1; x <= size; x++) {
			c[x - 1] = (char) (x + 48); //48 is start of ascii for numerics
		}

		return permutate(c,size,0);
	}
	
	//recursive function to shift the first character along by one
	private static TreeSet<Integer> permutate(char[] c, int size, int posReset) {
		TreeSet<Integer> res = new TreeSet<Integer>();
		int pos=0;
		for (int i = 0; i< size; i++) {
			char x = c[pos];
			char y = c[pos+1];
			c[pos+1]=x;
			c[pos]=y;
			res.add(Integer.parseInt(new String(c)));			
			if (++pos==size-1) pos=posReset;
			//recursively get the next set of permutations for the string
			if (posReset<size-2) res.addAll(permutate(c,size,posReset+1));
		}				
		return res;
		
	}


	// simple check to see if each number is present only once - i.e. a valid "pandigital"
	private static boolean isPanDigital(int number) {
		String t = String.valueOf(number);
		for (int i = t.length(); i >= 1; i--) {
			char s = (char) (i + 48);
			int x = t.indexOf(s);
			if (x == -1 || (t.indexOf(s, x + 1) != -1)) {
				return false;
			}
		}
		return true;
	}

}
