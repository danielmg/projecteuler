package euler.problem22;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

public class Problem22 {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {

		new Problem22().execute();

	}

	public void execute() throws IOException {
		InputStream is = this.getClass().getResourceAsStream("/names.txt");
		StringBuilder b = new StringBuilder();
		try {
			// read in the file
			int buffSize = 1024;
			byte[] buff = new byte[buffSize];
			while (buffSize == 1024) {
				buffSize = is.read(buff);
				b.append(new String(buff, 0, buffSize));
			}
			// split on delimiter
			String[] names = b.toString().replaceAll("\"", "").split(",");

			String[] n=names.clone();
			long startTimeJava = System.nanoTime();
			Arrays.sort(n);
			long elapsedJava = System.nanoTime() - startTimeJava;
			
			long startTimeDan = System.nanoTime();
			// sort names
			sortStrings(names);
			long elapsedDan = System.nanoTime() - startTimeDan;

		

			System.out.println("sort times: java - " + elapsedJava
					/ 1000000000d + " dan - " + elapsedDan / 1000000000d);

			// get total
			long total = 0l;
			long x = 0;
			for (String s : names) {
				x++;
				int cVal = 0;
				for (char c : s.toLowerCase().toCharArray())
					cVal += ((int) c) - 96;
				// System.out.println(s + " " + (x * cVal) + " " + x + " " +
				// cVal);
				total += x * cVal;
			}
			System.out.println(total);
		} finally {
			if (is != null)
				is.close();
		}
	}

	private static boolean DEBUG_OUTPUT = false;

	private void sortStrings(String[] strings) {

		Node tree = buildTree(strings);

		// now sort each layer of the tree
		sortTree(tree);

		if (DEBUG_OUTPUT) outputTree(tree, 0);
		
		// now recombine the tree into an array
		this.sortPos = 0;
		exportTree(tree, strings, "");		
	}

	private int sortPos = 0;

	private void exportTree(Node tree, CharSequence[] array, String curr) {
			String s = curr;
			for (int x=0;x<tree.getChildCount();x++) {
				Node c = tree.getChildren()[x];
				if (c.isFull()) array[sortPos++] = s + c.getKey();
				exportTree(c, array, s + c.getKey());
			}
	}

	private void sortTree(Node tree) {
		if (tree.getChildCount()>0) {
			this.quickSortPartition(tree.getChildren(), 0,tree.getChildCount() - 1);
			for (int x=0;x<tree.getChildCount();x++) {
				sortTree(tree.getChildren()[x]);
			}
		}
	}

	private void outputTree(Node tree, int depth) {
		int curr = depth + 1;
		for (int x=0;x<tree.getChildCount();x++) {
			Node n = tree.getChildren()[x];
			if (curr == 1) {
				System.out.print('+');
			} else {
				for (int i = 0; i < curr; i++)
					System.out.print('-');
			}
			System.out.println(n.getKey() + " " + n.isFull());
			outputTree(n, curr);
		}		
	}

	private Node buildTree(CharSequence[] strings) {

		Node root = new Node('\0', false);
		
		// determine depth
		int depth = 0;
		for (CharSequence s : strings) {
			if (s.length() > depth)
				depth = s.length();
		}

		for (CharSequence s : strings) {
			Node curr = root;
			for (int i = 1; i <= depth; i++) {
				if (s.length() >= i) {
					char key = s.charAt(i - 1);
					if (curr.getChild(key) == null) {
						Node n = new Node(key, i == s.length());
						curr.addChild(n);
						curr = n;
					} else {
						curr = curr.getChild(key);
						if (s.length()==i) curr.setFull(true);
					}
				}
			}
		}

		return root;
	}

	private void quickSortPartition(final Node[] arr, final int left,
			final int right) {

		int x = left;
		int y = right;
		final char p = arr[left + (right - left) / 2].getKey();
		while (x <= y) {
			while (arr[x].getKey() < p)
				x++;
			while (arr[y].getKey() > p)
				y--;
			if (x <= y) {
				Node z = arr[x];
				arr[x++] = arr[y];
				arr[y--] = z;
			}
		}

		if (left < y) quickSortPartition(arr, left, y);
		if (x < right) quickSortPartition(arr, x, right);

	}

	private class Node {
		private char key;
		private Node[] children;
		private int childCount=0;
		private boolean full;
		
		public int getChildCount() {
			return childCount;
			}
		public Node(char key, boolean full) {
			this.key = key;
			this.full = full;
		}

		private void setFull(boolean full) {
			this.full = full;
		}

		public boolean isFull() {
			return full;
		}

		public char getKey() {
			return key;
		}

		public Node getChild(char key) {
			for (int i =0;i<this.childCount;i++) {
					if (children[i].getKey() == key) return children[i];
			}
			return null;
		}

		public Node[] getChildren() {
			return children;
		}


		public void addChild(Node child) {
			if (this.childCount==0) {
				// initialise
				this.children = new Node[++this.childCount];
			} else {
				// resize
				Node[] t = new Node[this.childCount + 1];
				System.arraycopy(this.children, 0, t, 0, this.childCount++);
				this.children = t;
			}
			this.children[this.childCount - 1] = child;			
		}

	}

}
