package euler.problem17;

import java.util.HashMap;
import java.util.Map;

public class Problem17 {

	/**
	 * @param args
	 *            If the numbers 1 to 5 are written out in words: one, two,
	 *            three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19
	 *            letters used in total.
	 * 
	 * If all the numbers from 1 to 1000 (one thousand) inclusive were written
	 * out in words, how many letters would be used?
	 * 
	 * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and
	 * forty-two) contains 23 letters and 115 (one hundred and fifteen) contains
	 * 20 letters. The use of "and" when writing out numbers is in compliance
	 * with British usage.
	 * 
	 */
	private Map<Integer, String> map = new HashMap<Integer, String>();
	{
		map.put(1, "One");
		map.put(2, "Two");
		map.put(3, "Three");
		map.put(4, "Four");
		map.put(5, "Five");
		map.put(6, "Six");
		map.put(7, "Seven");
		map.put(8, "Eight");
		map.put(9, "Nine");
		map.put(10, "Ten");
		map.put(11, "Eleven");
		map.put(12, "Twelve");
		map.put(13, "Thirteen");
		map.put(14, "Fourteen");
		map.put(15, "Fifteen");
		map.put(18, "Eighteen");
		map.put(20, "Twenty");
		map.put(30, "Thirty");
		map.put(40, "Forty");
		map.put(50, "Fifty");
		map.put(60, "Sixty");
		map.put(70, "Seventy");
		map.put(80, "Eighty");
		map.put(90, "Ninety");
	
	}

	
	public static void main(String[] args) {

		new Problem17().do17();

	}

	public void do17() {
		StringBuilder b = new StringBuilder();
		for (int i = 1; i <= 1000; i++) {

			if (i < 100) {
				b.append(getLowNum(i));
			} else if (i >= 100 && i < 1000) {
				if (i % 100 == 0) {
					b.append(map.get(i / 100));
					b.append("hundred");
				} else {
					b.append(map.get((i - i % 100) / 100));
					b.append("hundredand");
					b.append(getLowNum(i % 100));
				}
			} else if (i == 1000) {
				b.append("OneThousand");
			}
		}
		System.out.println(b);
		System.out.println(b.length());
	}

	private String getLowNum(int i) {
		if (i < 16 || i == 20 || i == 30 || i == 40 || i == 50|| i == 60 || i == 70 || i == 80|| i == 90) {
			return map.get(i);
		} else if (i < 20) {
			if (i == 18) {
				// stupid number doesn't follow rules :(
				return map.get(18);
			} else {
				return map.get(i - 10) + "teen";
			}
		} else if (i > 20 && i < 100 && i % 10 != 0) {
			return (map.get(i - i % 10)) + map.get(i % 10);
		}		
		return "This number is too damn high " + i;
	}

}
