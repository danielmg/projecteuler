package euler.problem20;

import java.math.BigInteger;

public class Problem20 {


	public static void main(String[] args) {
		long startTime = System.nanoTime();	
		BigInteger b = BigInteger.valueOf(100);
		for (int i = 99;i>0;i--) {
			b=b.multiply(BigInteger.valueOf(i));
		}
		int sum=0;
		System.out.println("*" + b);
		for (char c: b.toString().toCharArray()) {
			sum+=(c-48);
		}
		long endTime = System.nanoTime();
		System.out.println("**" + sum);		
		System.out.println("elapsed: " + (endTime-startTime) + "ns ");
	}

}
