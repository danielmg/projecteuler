package euler.problem9;
/**
 * A Pythagorean triplet is a set of three natural numbers, a  b  c, for which,
* a^2 + b^2 = c^2
* For example, 32 + 42 = 9 + 16 = 25 = 52.
* There exists exactly one Pythagorean triplet for which a + b + c = 1000.
* Find the product abc.
 */
public class Problem9 {
	
	public static void main(String[] args) {
		long startTime = System.nanoTime();	
		long answer=0;
		for (int a=1;a< 500;a++) {
			boolean found=false;
			for (int b=a+1;b< 500;b++) {
				double d = Math.sqrt((a*a+b*b));
				long c = Math.round(d);
				if (d==c&&c>b&&a+b+c==1000) {
					System.out.println("a "+a+" b "+b+" c "+c);
					answer=a*b*c;
					found=true;
					break;
				}
			}
			if (found) break;
		}
		long endTime = System.nanoTime();
		System.out.println("answer:" +answer);
		System.out.println("elapsed: " + (endTime-startTime) + "ns ");
	}	
	
	
	
}
