package euler.problem3;

public class Problem3 {

	/**
	 * @param args
	 * The prime factors of 13195 are 5, 7, 13 and 29.
	 * What is the largest prime factor of the number 600851475143 ?
	 */	 	
	public static void main(String[] args) {
		long startTime = System.nanoTime();
		long number=600851475143l;
		long i = 3;
		while (i * i <= number) {
			if (number % i == 0) {
				number /= i;
			} else {
				i += 2;
			}
		}
		long endTime = System.nanoTime();
		//output our remaining prime number
		System.out.println("Answer:" + number);
		System.out.println("elapsed: " + (endTime-startTime) + "ns");
		//6857
	}

}
