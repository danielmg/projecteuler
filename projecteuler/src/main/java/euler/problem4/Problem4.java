package euler.problem4;

/**
 * A palindromic number reads the same both ways. The largest palindrome made
 * from the product of two 2-digit numbers is 9009 = 91 99 (91*99) Find the
 * largest palindrome made from the product of two 3-digit numbers.
 */
public class Problem4 {

	public static void main(String[] args) {
		int pal = 999;
		int paly = 0;
		int palx = 0;		
		long startTime = System.nanoTime();
		for (int x = 999; x > 99; x--) {
			//palindrome factors cannot be 10
			if (x%10!=0) {
				//loop back to the y factor of last plaindrome
				for (int y = 999; y > pal/x; y--) {
					int testPal=x*y;
					if ((y%10!=0)&& isPalindrome(""+testPal)){					
						if (testPal>pal) {
							pal = testPal;
							palx = x;
							paly = y;
							break;
						}
					}
				}
			}	
		}
		long endTime = System.nanoTime();
			
		System.out.println("answer: " + pal + "(" + palx + "*" + paly + ")");		
		System.out.println("elapsed: " + (endTime-startTime) + "ns ");
		//answer: 906609(913*993)
	}

	private static boolean isPalindrome(String p) {
		for (int i = 0; i< p.length()/2;i++) {			
			if (p.charAt(i)!=p.charAt(p.length()-i-1)) return false;
		}
		return true; 
	}
	
	
}