package euler.problem16;

public class Problem16 {

	/**
	 * @param args
	 *            2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 =
	 *            26. What is the sum of the digits of the number 2^1000?
	 * 
	 */
	public static void main(String[] args) {

		// first we need to find the product of 2*2 etc etc
		// this is going to be too big to fit in the registers so
		// we need to do some artbitary maths - but not using
		// BigDecimal as that is cheating!
		int[] number = new int[] { 2 };
		int[] multi = new int[] { 2 };

		for (int i = 0; i < 999; i++) {
			number = multiply(number, multi);
		}

		// now we need to sum up the digits
		long sum = 0;
		for (int i : number) {
			sum += i;
		}

		System.out.println("answer: " + sum);

	}

	private static int[] multiply(int[] num1, int[] num2) {

		// multiply each value in num1 against each value in num2
		// most it can be is the longest number length *2
		int[] ans = new int[(num1.length > num2.length ? num1.length
				: num2.length) * 2];

		int c = 0;
		int decPlace = 0;

		// why do we write numbers backwards??
		for (int xPos = num1.length - 1; xPos >= 0; xPos--) {
			int x = num1[xPos];
			c = decPlace++;
			// carry over any extra into the next decimal place
			int carry = 0;
			for (int yPos = num2.length - 1; yPos >= 0; yPos--) {
				int y = num2[yPos];
				int sum = x * y + carry + ans[c];
				if (sum > 9) {
					carry = (sum - sum % 10) / 10;
					sum = sum % 10;
				} else {
					carry = 0;
				}
				ans[c] = sum;
				// move the answer pointer on.
				c++;
			}
			if (carry > 0) {
				ans[c] = carry;
			} else {
				c--;
			}

		}
		if (c < ans.length) {
			// truncate the array then invert
			int[] tAns = new int[c+1];
			for (int i = 0; i <= c; i++) {
				tAns[i] = ans[c - i];
				//System.out.print(tAns[i]);
			}
			ans = tAns;
		}
		//System.out.println();
		return ans;
	}

}
