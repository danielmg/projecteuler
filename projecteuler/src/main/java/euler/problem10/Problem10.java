package euler.problem10;


public class Problem10 {

	/**
	 * @param args
	 *            The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
	 * 
	 * Find the sum of all the primes below two million.
	 * 
	 */
	public static void main(String[] args) {
		Problem10 p10 = new Problem10();
		p10.bruteAnswer();
		// answer: 21365272
		//1179908154

	}

	public void bruteAnswer() {
		long startTime = System.nanoTime();
		long answer = 2;
		for (int x = 3; x < 2000000; x += 2) {
			boolean isPrime = true;
			for (int i = 3; i*i <= x; i += 2) {
				if (x % i == 0) {
					isPrime = false;
					break;
				}
			}
			if (isPrime)
				answer += x;
		}
		long endTime = System.nanoTime();
		System.out.println("Brute answer:" + answer);
		System.out.println("elapsed: " + (endTime - startTime) + "ns ");
	}
}
