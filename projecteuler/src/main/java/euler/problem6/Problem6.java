package euler.problem6;

public class Problem6 {

	/**
	 * @param args
	 *            The sum of the squares of the first ten natural numbers is, 1^2 +
	 *            2^2 + ... + 10^2 = 385 The square of the sum of the first ten
	 *            natural numbers is,
	 * 
	 * (1 + 2 + ... + 10)2 = 552 = 3025 Hence the difference between the sum of
	 * the squares of the first ten natural numbers and the square of the sum is
	 * 3025 385 = 2640.
	 * 
	 * Find the difference between the sum of the squares of the first one
	 * hundred natural numbers and the square of the sum.
	 * 
	 */
	public static void main(String[] args) {
		int sumSquares=0;
		int sum=0;
		long startTime = System.nanoTime();		
		for (int i = 1; i<=100;i++) {
			sum+=i;
			sumSquares+=i*i;
		}
		long endTime = System.nanoTime();
		System.out.println("answer:" +(sum*sum-sumSquares));
		System.out.println("elapsed: " + (endTime-startTime) + "ns ");		
	}

}
