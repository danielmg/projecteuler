package euler.problem14;


public class Problem14 {

	/**
	 * @param args
	 * n  n/2 (n is even)
	 * n  3n + 1 (n is odd)
	 */
	public static void main(String[] args) {
		int largestCount=0;
		int largestI=0;
		for (int i=1;i<1000000;i++) {
			long n=i;
			int count=1;
			while (n!=1) {
				if (n%2==0) {
					//even
					n/=2;
				} else {
					//odd
					n=3*n+1;					
				}				
				count++;	
			}
			if (count>largestCount) {
				largestI=i;
				largestCount=count;				
			}
			
		}
		
		System.out.println(largestI+ " ("+largestCount+")");
	}

	

}
