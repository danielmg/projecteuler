package euler.problem15;

public class Problem15 {

	/**
	 * @param args
	 *            Starting in the top left corner of a 2x2 grid, there are 6
	 *            routes (without backtracking) to the bottom right corner.
	 * 
	 * How many routes are there through a 20x20 grid?
	 */
	public static void main(String[] args) {
		int xMax = 20;
		int yMax = 20;
		long c = 1;

		for (int x = 1; x <= xMax; x++) {			
			c = (c * (yMax*2 + 1 - x)) / x;			
		}		
		System.out.println(" " + c);
		
		//answer 137846528820
	}

}
