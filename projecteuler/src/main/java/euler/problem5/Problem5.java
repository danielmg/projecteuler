package euler.problem5;

public class Problem5 {

	/**
	 * @param args
	 * 
	 * 2520 is the smallest number that can be divided by each of the numbers
	 * from 1 to 10 without any remainder. What is the smallest positive number
	 * that is evenly divisible by all of the numbers from 1 to 20?
	 * 
	 */
	public static void main(String[] args) {
		
		int target = 20;		
		int c = target*target*target;
		long startTime = System.nanoTime();		
		while (true) {			
			int i = target;
			while (--i > 0) {				
				if (c % i != 0) {					
					break;
				}
			}
			if (i>0) {							
				c+=target;			
			} else {
				break;
			}
		}
		long endTime = System.nanoTime();
		System.out.println(c);
		System.out.println("elapsed: " + (endTime-startTime) + "ns ");
		// answer = 232792560
		//70039249ns

	}

}
