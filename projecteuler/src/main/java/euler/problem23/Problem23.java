package euler.problem23;




/**
 * @author daniel.grout
 *
 * A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. 
 * For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 
 * is a perfect number.  A number n is called deficient if the sum of its proper divisors is less than n 
 * and it is called abundant if this sum exceeds n.  As 12 is the smallest abundant number, 
 * 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. 
 * By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum 
 * of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though 
 * it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less 
 * than this limit.
 * 
 * Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
 *
 */
public class Problem23 {

	private static final int UPPER_LIMIT=20161;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Problem23 p23 = new Problem23();
		long start=System.nanoTime();
		long ans=p23.doWork();
		long elapsed = System.nanoTime()-start;
		System.out.println("answer: " + ans);
		System.out.println("elapsed: " + elapsed/1000000000d);
	}
	//4110061
	public long doWork() {

		boolean[] abuns = new boolean[UPPER_LIMIT];		
		for (int n=12;n<UPPER_LIMIT;n++) for (int i=2,x=3, max=(int)Math.sqrt(n);i<max;i++)  if( abuns[n]=(x=x+((n%i==0)?(i+(max=n/i)-1):0))>n) break;								
				
		long sum=0;
		boolean summed=false;
		for (int i=1;i<UPPER_LIMIT;i++,summed=false) {			
			for (int x=12; x<i/2+1;x++) if (summed=abuns[x]&&abuns[i-x]) break;			
			if (!summed) sum+=i;
		}		
		return sum;
	}

	
}

