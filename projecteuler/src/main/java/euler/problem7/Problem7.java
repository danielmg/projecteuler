package euler.problem7;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

public class Problem7 {

	/**
	 * @param args
	 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
	 *
	 * What is the 10 001st prime number?
	 *
	 */
	public static void main(String[] args) {
		Problem7 p7 = new Problem7();
		p7.naiveAnswer();		
		p7.sieveAnswer();
	}
	
	public class Sieve
	{
	    private BitSet sieve;
	 
	    public Sieve(int size) {
	        sieve = new BitSet((size+1)/2);
	    }
	 
	    public boolean is_composite(int k)
	    {
	        assert k >= 3 && (k % 2) == 1;
	        return sieve.get((k-3)/2);
	    }
	 
	    public void set_composite(int k)
	    {
	        assert k >= 3 && (k % 2) == 1;
	        sieve.set((k-3)/2);
	    }
	 


	}

	private List<Integer> sieveOfEratosthenes(int max)
	{
		Sieve sieve = new Sieve(max + 1); // +1 to include max itself
		for (int i = 3; i*i <= max; i += 2) {
		    if (sieve.is_composite(i))
		        continue;
		 
		    // We increment by 2*i to skip even multiples of i
		for (int multiple_i = i*i; multiple_i <= max; multiple_i += 2*i)
		        sieve.set_composite(multiple_i);
		}
		
	    List<Integer> primes = new ArrayList<Integer>();
	    primes.add(2);
	    for (int i = 3; i <= max; i += 2)
	        if (!sieve.is_composite(i))
	            primes.add(i);
	    return primes;
	}

	
	
	public  void sieveAnswer() {
		long startTime = System.nanoTime();		
		int max=999999;
	
		int answer=sieveOfEratosthenes(max).get(10000);
		
		long endTime = System.nanoTime();
		System.out.println("Sieve answer:" +answer);
		System.out.println("elapsed: " + (endTime-startTime) + "ns ");
	}
	

	public void naiveAnswer() {
		
		long test=0;
		int c=0;
		long startTime = System.nanoTime();		
		while(c<=10001) {
			if (isPrimeNaive(++test)) {
				c++;
			}
		}
		long endTime = System.nanoTime();
		System.out.println("Naive answer:" +test);
		System.out.println("elapsed: " + (endTime-startTime) + "ns ");
		
		//answer:104743
	}
	
	private static boolean isPrimeNaive(long number) {
		//naive prime test
		if (number==2) return true;
		if (number%2==0) return false;
		for (long i=3;i<Math.sqrt(number)+1;i++) {
			if (number%i==0) return false;
		}
		return true;
	}
	
	

}
