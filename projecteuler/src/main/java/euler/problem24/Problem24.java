package euler.problem24;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


/**
 * @author daniel.grout
 *
 * A permutation is an ordered arrangement of objects. For example, 3124 is one possible 
 * permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically 
 * or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:
 * 
 * 012   021   102   120   201   210
 *
 * What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
 *
 */
public class Problem24 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Problem24 p24 = new Problem24();
		long start=System.nanoTime();
		String ans=p24.doWork();
		long elapsed = System.nanoTime()-start;
		System.out.println("answer: " + ans);
		System.out.println("elapsed: " + elapsed/1000000000d);
	}

	public String doWork() {
		int[] arr=new int[]{0,1,2,3};
		List<String> nums = new LinkedList<String>();;				
	
		for (int x=0;x<arr.length;x++)
		for (int i=x;i<arr.length;i++) {
			int t=arr[i];
			if (i==arr.length-1) {							
				arr[i]=arr[0];
				arr[0]=t;
			} else {
				arr[i]=arr[i+1];
				arr[i+1]=t;
			}
				nums.add(toString(arr));	
			
			}
	
		
		Collections.sort(nums);
		for (String s:nums) System.out.println(s);
		return "";
	}
	
	private void perm(int[] arr,int x,Collection<String> n) {
			
 			//perm(arr, x+1, n);
		
	}

	private String toString(int[] arr) {
		String s="";
		for (int i:arr) s+=i;
		return s;
	}
	
}
