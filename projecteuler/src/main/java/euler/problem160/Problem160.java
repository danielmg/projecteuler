package euler.problem160;

public class Problem160 {

	/**
	* For any N, let f(N) be the last five digits before the trailing zeroes in N!.
	* For example,
	* 
	* 9! = 362880 so f(9)=36288
	* 10! = 3628800 so f(10)=36288
	* 20! = 2432902008176640000 so f(20)=17664
	* 
	* Find f(1,000,000,000,000)
	* 
	 */
	public static void main(String[] args) {
		long target   = 1000000000000l;
		System.out.println(fact(target));
	}

	private static long fact(long n) {
		long sum=1;
		for (long i=1;i<=n;i++) {
			sum*=i;
			if (sum%10==0) sum/=10;
			sum%=100000;
		}
		return sum;
	}
}
